package
{
	import flash.display.Sprite;
	
	public class TestEasyLog extends Sprite
	{
		public function TestEasyLog()
		{
			log("简单的使用 log() 即可显示log的来源对象");
			logBy({}, "使用 logBy() 指定log的来源对象");
			testStatic();
			logRegNickName(this, "主程序");
			log("使用 logRegNickName() 为来源对象指定昵称");
			logRegNickName(this, null);
			log("使用 logRegNickName(target, null) 清除已注册的昵称");
		}
		
		public static function testStatic():void
		{
			log("这是从static方法中log出的结果");
		}
	}
}