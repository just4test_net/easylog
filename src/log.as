package
{
	import avmplus.getQualifiedClassName;

	[Inline]
	/**
	 *全局log方法。使用inline以便获取this。
	 * 必须在编译时指定 -inline 编译指令。
	 * log只能使用单一参数，因为inline方法不能使用参数数组和可省略参数。
	 */
	public function log(info:String):void
	{
		//判断是否未开启inline
		if(!logSupport.notFirst)
		{
			if("global" == getQualifiedClassName(this))
			{
				logBy("[EasyLog]", "未在编译时指定 -inline 参数，EasyLog无法正常工作。")
				logSupport.global = this;
			}
			logSupport.notFirst = true;
		}
		
		//依然判断一次，防止虽然开了inline，但第一次真的是从global唤起了log方法。
		if(this === logSupport.global)
			logBy(null, info);
		else
			logBy(this, info);
	}
}

class logSupport
{
	/**
	 *记录log是否第一次运行。如果-inline没有开启，log的this将永远为global。 
	 */
	static var notFirst:Boolean;
	static var global:*;
}