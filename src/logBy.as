package 
{
	import flash.display.Stage;

	public function logBy(target:Object, ...args):void
	{
		if(null == target)
		{
			trace(args);
		}
		else
		{
			var targetName:String = (Stage.prototype.__easyLog_nickNameMap && Stage.prototype.__easyLog_nickNameMap[target]) || target.toString();
			trace(targetName, ":", args);
		}
	}
}