package
{
	import flash.display.Stage;
	import flash.utils.Dictionary;
	
	public function logRegNickName(target:Object, nickName:String):void
	{
		//把Stage.prototype当做全局变量以便在全局方法中共享。要不还得声明一个类，多一个全局文件。
		Stage.prototype.__easyLog_nickNameMap ||= new Dictionary(true);
		Stage.prototype.__easyLog_nickNameMap[target] = nickName;
	}
}